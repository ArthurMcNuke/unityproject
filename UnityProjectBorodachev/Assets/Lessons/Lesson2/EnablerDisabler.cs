﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EnablerDisabler : MonoBehaviour
{
    private float seconds;
    [SerializeField] GameObject obj;
    void Start()
    {
        string path = @"StreamingAssets\File.json";
        string jsonString = string.Empty;

        if (File.Exists(path))
        {
            jsonString = File.ReadAllText(path, System.Text.Encoding.UTF8);
            PlayerInfo info = JsonUtility.FromJson<PlayerInfo>(jsonString);
            seconds = info.reEnableTime;
        }
        else
        {
            Debug.Log($"Cannot find file {path}");
        }

        StartCoroutine(EnableCoroutine());
    }
    private IEnumerator EnableCoroutine()
    {
        while (true) {
            yield return new WaitForSecondsRealtime(seconds);
            obj.SetActive(!obj.activeSelf);
        }
        
    }
}
