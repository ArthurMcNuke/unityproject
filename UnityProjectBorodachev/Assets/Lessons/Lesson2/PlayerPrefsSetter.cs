﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsSetter : MonoBehaviour
{
    void Awake()
    {
        PlayerPrefs.SetFloat("ReEnableTime", 1.0f);
        PlayerPrefs.Save();
    }
    private void OnDestroy()
    {
        PlayerPrefs.DeleteKey("ReEnableTime");
        PlayerPrefs.Save();
    }
}
