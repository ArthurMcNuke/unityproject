﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    [SerializeField] Vector3 spawnPosition;
    [SerializeField] GameObject prefab;
    void Awake()
    {
        Instantiate(prefab, spawnPosition, Quaternion.identity);
    }
}
