﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetComponentScript : MonoBehaviour
{
    Transform otherTransform;
    void Start()
    {
        EnablerDisabler obj = FindObjectOfType<EnablerDisabler>();
        obj?.TryGetComponent<Transform>(out otherTransform);
    }
}
