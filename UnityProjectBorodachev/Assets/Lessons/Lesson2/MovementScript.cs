﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 moveDir = new Vector3(horizontalInput, 0, verticalInput).normalized;
        transform.Translate(moveDir * moveSpeed * Time.deltaTime);
    }
}
