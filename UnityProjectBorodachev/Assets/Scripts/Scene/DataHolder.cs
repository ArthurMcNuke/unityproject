﻿using UnityEngine;

/// <summary>
/// Содержит данные влияющие на ввод игрока
/// </summary>
public class DataHolder: MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] float mouseSensitivity;
    [SerializeField] float jumpHeight;

    /// <summary>
    /// Возвращает скорость передвижения игрока
    /// </summary>
    public float MoveSpeed { get { return moveSpeed; } }
    /// <summary>
    /// Возвращает чувствительность мыши
    /// </summary>
    public float MouseSensitivity { get { return mouseSensitivity; } }
    /// <summary>
    /// Возвращает высоту прыжка
    /// </summary>
    public float JumpHeight { get { return jumpHeight; } }
}
