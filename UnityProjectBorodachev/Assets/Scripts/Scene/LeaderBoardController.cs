﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LeaderBoardController : MonoBehaviour
{
    private List<LeaderBoardItem> LeaderBoard = new List<LeaderBoardItem>();
    private TimerController timer;

    private void OnDisable()
    {
        timer.OnTimerEnded -= SaveCurrentResult;
    }

    private void Start()
    {
        timer = FindObjectOfType<TimerController>();
        timer.OnTimerEnded += SaveCurrentResult;
        ReadData();
    }

    private void SaveCurrentResult()
    {
        ScoreCounter sc = FindObjectOfType<ScoreCounter>();

        InsertItem(sc.Score);
    }

    private void ReadData()
    {
        string path = Path.Combine(Application.dataPath, "Resources");
        for (int i = 0; i < 5; i++)
        {
            string fileName = $"BestPlayer{i + 1}.json";
            string jsonString = File.ReadAllText(Path.Combine(path, fileName), System.Text.Encoding.UTF8);
            LeaderBoardItem item = JsonUtility.FromJson<LeaderBoardItem>(jsonString);
            LeaderBoard.Add(item);
        }
    }

    private void InsertItem(int score)
    {
        for (int i = 0; i < LeaderBoard.Count; i++)
        {
            if (score > LeaderBoard[i].Score)
            {
                LeaderBoardItem newItem = new LeaderBoardItem();
                newItem.Name = PlayerPrefs.GetString("CurrentPlayer");
                newItem.Score = score;

                LeaderBoard.Insert(i, newItem);
                LeaderBoard.RemoveAt(LeaderBoard.Count - 1);
                WriteData();
                break;
            }
        } 
    }

    private void WriteData()
    {
        string path = Path.Combine(Application.dataPath, "Resources");
        for (int i = 0; i < LeaderBoard.Count; i++)
        {
            string fileName = $"BestPlayer{i + 1}.json";
            File.WriteAllText(Path.Combine(path, fileName), JsonUtility.ToJson(LeaderBoard[i]), System.Text.Encoding.UTF8);
        }
    }
}
