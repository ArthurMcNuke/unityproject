﻿/// <summary>
/// Запись таблицы лучших игроков
/// </summary>
[System.Serializable]
public class LeaderBoardItem
{
    /// <summary>
    /// Имя Игрока
    /// </summary>
    public string Name;

    /// <summary>
    /// Количество очков
    /// </summary>
    public int Score;
}
