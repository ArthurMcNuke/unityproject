﻿using System;
using System.Collections;
using UnityEngine;

public class TimerController : MonoBehaviour
{
    /// <summary>
    /// Действия производимые при старте таймера
    /// </summary>
    public Action<string> OnTimerStarted = delegate { };
    /// <summary>
    /// Действия производимые после отсчёта очередной секунды
    /// </summary>
    public Action<string> OnTimerChanged = delegate { };
    /// <summary>
    /// Действия производимые по оканчанию времени
    /// </summary>
    public Action OnTimerEnded = delegate { };

    [SerializeField] int timeSeconds;

    Coroutine timer;

    private void Start()
    {
        if (timer != null)
        {
            StopCoroutine(timer);
        }
        else
        {
            timer = StartCoroutine(TimerCoRoutine());
        }
    }

    private IEnumerator TimerCoRoutine()
    {
        OnTimerStarted.Invoke(FormatTime());
        while (timeSeconds > 0)
        {
            yield return new WaitForSeconds(1.0f);
            timeSeconds -= 1;
            OnTimerChanged.Invoke(FormatTime());
        }

        OnTimerEnded.Invoke();
    }

    private string FormatTime()
    {
        return TimeSpan.FromSeconds(timeSeconds).ToString();
    }
}
