﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] SimpleSpawner spawner;
    [SerializeField] List<Vector3> spawnPositions;

    [SerializeField] Vector3 lowerLeftCornerOfTheRoom;
    [SerializeField] Vector3 upperRightCornerOfTheRoom;

    private Vector3 destination = Vector3.zero;

    private List<AbstractEnemy> enemies = new List<AbstractEnemy>();

    Coroutine moveEnemies;

    private void Start()
    {
        foreach (Vector3 pos in spawnPositions)
        {
            AbstractEnemy enemy = spawner.SpawnEnemy(pos);
            if (enemy != null)
            {
                enemies.Add(enemy);
            }
        }

        if (moveEnemies != null)
        {
            StopCoroutine(moveEnemies);
        }
        else
        {
            moveEnemies = StartCoroutine(MoveEnemies());
        }
    }

    private IEnumerator MoveEnemies()
    {
        while (enabled)
        {
            foreach (AbstractEnemy enemy in enemies)
            {
                if (enemy.gameObject.activeInHierarchy == false)
                {
                    spawner.SpawnEnemy(spawnPositions[Random.Range(0, spawnPositions.Count)]);
                }
                    SetEnemyDestination(enemy);
                yield return new WaitForSeconds(0.5f);
            }

            yield return new WaitForSeconds(Random.Range(3, 6));
        }
    }

    private void SetEnemyDestination(AbstractEnemy enemy)
    {
        destination.x = Random.Range(lowerLeftCornerOfTheRoom.x, upperRightCornerOfTheRoom.x);
        destination.y = lowerLeftCornerOfTheRoom.y;
        destination.z = Random.Range(lowerLeftCornerOfTheRoom.z, upperRightCornerOfTheRoom.z);
        Physics.Raycast(destination, Vector3.down, out RaycastHit hit, 15.0f, 0xff, QueryTriggerInteraction.UseGlobal);
        enemy.MeshAgent.SetDestination(hit.point);
    }
}
