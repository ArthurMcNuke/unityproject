﻿using UnityEngine;

public class PauseScript : MonoBehaviour
{
    [SerializeField] Canvas gamePauseCanvasPrefab;
    private Canvas gamePauseCanvas = null;
    private PlayerActions pa;

    private void Start()
    {
        pa = FindObjectOfType<PlayerActions>();
        PlayerActions.OnEscapePressed += PauseGameplay;
    }

    private void OnDisable()
    {
        PlayerActions.OnEscapePressed -= PauseGameplay;
    }

    /// <summary>
    /// Приостанавливает игровой процесс
    /// </summary>
    public void PauseGameplay()
    {
        if (gamePauseCanvas == null)
        {
            gamePauseCanvas = Instantiate<Canvas>(gamePauseCanvasPrefab);
        }

        gamePauseCanvas.gameObject.SetActive(true);
        pa.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0.0f;
    }

    /// <summary>
    /// Воспроизводит игровой процесс
    /// </summary>
    public void ResumeGameplay()
    {
        gamePauseCanvas.gameObject.SetActive(false);
        pa.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1.0f;
    }
}
