﻿using System;
using UnityEngine;
using System.IO;

/// <summary>
/// Класс начисляющий очки за попадания по врагу
/// </summary>
public class ScoreCounter : MonoBehaviour
{
    int score = 0;
    /// <summary>
    /// Действия производимые при попадании в цель
    /// </summary>
    public event Action<int> OnTargetHit = delegate (int score) { };
    ScoreConfig config;

    private void Start()
    {
        string path = Path.Combine(Application.dataPath, "Resources", "ScoreConfig.json");

        if (File.Exists(path))
        {
            string jsonString = File.ReadAllText(path, System.Text.Encoding.UTF8);
            config = JsonUtility.FromJson<ScoreConfig>(jsonString);
        }
        else
        {
            Debug.LogError($"Cannot find file {path}");
        }
    }

    /// <summary>
    /// Возвращает количество очко начисленных за текущую сессию
    /// </summary>
    public int Score{ get { return score; } }

    /// <summary>
    /// Начисляет очки за попадание по врагу
    /// <para>Также начисляет дополнительные очки за убийство в соответствии с типом врага</para>
    /// </summary>
    /// <param name="hit">Рейкст выстрела</param>
    /// <param name="dmg">Урон стрелявшего оружия</param>
    public void CountHit(RaycastHit hit, int dmg)
    {
        if (hit.collider != null)
        {
            hit.collider.TryGetComponent<AbstractEnemy>(out AbstractEnemy enemy);
            if (enemy != null)
            {
                score += config.ScoreForHittingEnemy;
                switch (enemy)
                {
                    case SimpleTarget target:
                        target.Health.Decrease(dmg);

                        if (target.Health.HealthPoints <= 0)
                        {
                            score += config.ScoreForKillingSimpleTarget;
                        }

                        OnTargetHit.Invoke(score);
                        break;
                }
            }
        }
    }
}
