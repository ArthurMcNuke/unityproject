﻿/// <summary>
/// Класс содержащий данные по начислению очков
/// </summary>
[System.Serializable]
public class ScoreConfig
{
    /// <summary>
    /// Количество очков начисляемое за попадание по врагу
    /// </summary>
    public int ScoreForHittingEnemy;
    /// <summary>
    /// Количество очков начисляемое за убийство SimpleTarget
    /// </summary>
    public int ScoreForKillingSimpleTarget;
}
