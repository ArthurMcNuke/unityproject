﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HealthBar : MonoBehaviour
{
    private Player player;
    private Slider slider;

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    private void Start()
    {
        player = FindObjectOfType<Player>();
        player.Health.OnHPChanged += SetHealthBarValue;
    }

    private void SetHealthBarValue()
    {
         slider.value = player.Health.HealthPoints;
    }

    private void OnDisable()
    {
        player.Health.OnHPChanged -= SetHealthBarValue;
    }
}
