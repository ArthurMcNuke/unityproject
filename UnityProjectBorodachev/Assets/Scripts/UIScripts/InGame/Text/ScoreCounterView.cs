﻿public class ScoreCounterView : AbstractTextView
{
    private ScoreCounter scoreCounter;

    private void Start()
    {
        ScoreCounter scoreCounter = FindObjectOfType<ScoreCounter>();
        scoreCounter.OnTargetHit += ChangeText;
    }

    private void ChangeText(int score)
    {
        text.text = "Score: " + score.ToString();
    }
}
