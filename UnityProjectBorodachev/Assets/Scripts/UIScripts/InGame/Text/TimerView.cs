﻿using UnityEngine;

public class TimerView : AbstractTextView
{
    private TimerController timer;
    [SerializeField] Canvas gameOverWindow;

    private void Start()
    {
        timer = FindObjectOfType<TimerController>();
        timer.OnTimerStarted += ShowTime;
        timer.OnTimerChanged += ShowTime;
        timer.OnTimerEnded += GameOver;
    }

    private void OnDisable()
    {
        timer.OnTimerStarted -= ShowTime;
        timer.OnTimerChanged -= ShowTime;
        timer.OnTimerEnded -= GameOver;
    }

    private void ShowTime(string time)
    {
        text.text = time; 
    }

    private void GameOver()
    {
        Instantiate(gameOverWindow);
    }
}
