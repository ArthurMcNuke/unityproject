﻿using UnityEngine;

public class BulletsView : AbstractTextView
{
    private Player player;

    private void Start()
    {
        player = FindObjectOfType<Player>();
        player.Gun.OnAmmoChanged += Refresh;
    }

    private void OnDisable()
    {
        player.Gun.OnAmmoChanged -= Refresh;
    }

    private void Refresh()
    {
        text.text = player.Gun.ClipAmmo.ToString() + '/' + player.Gun.TotalAmmo.ToString();
    }
}
