﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonRestart : AbstractButtonView
{
    override protected void OnButtonClicked()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(1);
    }
}
