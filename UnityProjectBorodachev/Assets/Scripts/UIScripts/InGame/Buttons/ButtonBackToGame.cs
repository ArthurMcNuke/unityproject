﻿using UnityEngine;

public class ButtonBackToGame : AbstractButtonView
{
    private PauseScript pauser;

    private void Start()
    {
        pauser = FindObjectOfType<PauseScript>();
    }

    override protected void OnButtonClicked()
    {
        pauser.ResumeGameplay();
    }
}
