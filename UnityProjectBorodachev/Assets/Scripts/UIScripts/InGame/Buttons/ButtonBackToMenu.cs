﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonBackToMenu : AbstractButtonView
{
    override protected void OnButtonClicked()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(0);
    }
}
