﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class GameOverScript : MonoBehaviour
{
    private CanvasGroup canvasGroup = null;
    private Coroutine increaseOpacity = null;
    void Start()
    {
        PlayerActions pa = FindObjectOfType<PlayerActions>();
        pa.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        canvasGroup = GetComponent<CanvasGroup>();

        if (increaseOpacity != null)
        {
            StopCoroutine(increaseOpacity);
        }
        else
        {
            StartCoroutine(IncreaseOpacity());
        }
    }

    IEnumerator IncreaseOpacity()
    {
        while (canvasGroup.alpha < 1.0f)
        {
            canvasGroup.alpha += 0.01f;
            yield return new WaitForSecondsRealtime(0.01f);
        }

        canvasGroup.interactable = true;
    }
}
