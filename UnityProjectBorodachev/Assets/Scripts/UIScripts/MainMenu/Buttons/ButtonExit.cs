﻿using UnityEngine;

public class ButtonExit : AbstractButtonView
{
    protected override void OnButtonClicked()
    {
        Application.Quit();
    }
}
