﻿public class ButtonOptions : AbstractButtonView
{
    WindowManager wm;
    protected override void OnEnable()
    {
        wm = FindObjectOfType<WindowManager>();
        base.OnEnable();
    }

    protected override void OnButtonClicked()
    {
        wm.GoToOptionsWindow();
    }

   
}
