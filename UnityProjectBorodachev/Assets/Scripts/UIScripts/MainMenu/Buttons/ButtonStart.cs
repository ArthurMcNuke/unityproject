﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonStart : AbstractButtonView
{
    override protected void OnButtonClicked(){
        PlayerPrefs.SetInt("CurrentScore", 0);
        SceneManager.LoadScene(1);
    }
}
