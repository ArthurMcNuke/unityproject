﻿public class ButtonBestPlayers: AbstractButtonView
{
    WindowManager wm;
    override protected void OnEnable()
    {
        wm = FindObjectOfType<WindowManager>();
        base.OnEnable();
    }
    override protected void OnButtonClicked()
    {
        wm.GoToBestPlayersWindow();
    }
}
