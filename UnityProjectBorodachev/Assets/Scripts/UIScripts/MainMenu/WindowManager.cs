﻿using UnityEngine;

public class WindowManager : MonoBehaviour
{
    [SerializeField] Canvas mainMenuCanvas;
    [SerializeField] Canvas bestPlayersCanvas;
    [SerializeField] Canvas optionsCanvas;

    public void GoToBestPlayersWindow()
    {
        bestPlayersCanvas.gameObject.SetActive(true);
        mainMenuCanvas.gameObject.SetActive(false);
        optionsCanvas.gameObject.SetActive(false);
    }

    public void GoToMainMenuWindow()
    {
        mainMenuCanvas.gameObject.SetActive(true);
        bestPlayersCanvas.gameObject.SetActive(false);
        optionsCanvas.gameObject.SetActive(false);
    }

    public void GoToOptionsWindow()
    {
        optionsCanvas.gameObject.SetActive(true);
        bestPlayersCanvas.gameObject.SetActive(false);
        mainMenuCanvas.gameObject.SetActive(false);
    }

}
