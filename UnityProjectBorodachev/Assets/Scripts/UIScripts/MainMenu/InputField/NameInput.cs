﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class NameInput : MonoBehaviour
{
    InputField nameInput;
    private void Awake()
    {
        nameInput = GetComponent<InputField>();
        nameInput.text = PlayerPrefs.GetString("CurrentPlayer", "Player");
    }

    private void OnEnable()
    {
        nameInput.onEndEdit.AddListener(SaveName);
    }

    private void SaveName(string playerName)
    {
        PlayerPrefs.SetString("CurrentPlayer", playerName);
    }

    private void OnDisable()
    {
        nameInput.onEndEdit.RemoveListener(SaveName);
    }
}
