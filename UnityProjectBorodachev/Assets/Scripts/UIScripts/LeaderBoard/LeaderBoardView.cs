﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LeaderBoardView : MonoBehaviour
{
    private List<LeaderBoardItem> LeaderBoard = new List<LeaderBoardItem>();
    [SerializeField] Transform content;
    [SerializeField] LeaderBoardItemView itemPrefab;

    private void Start()
    {
        ReadData();
        FillLeaderBoard();
    }

    private void ReadData()
    {
        string path = @"Assets\Resources\";
        for (int i = 0; i < 5; i++)
        {
            string fileName = $"BestPlayer{i + 1}.json";
            string jsonString = File.ReadAllText(path + fileName, System.Text.Encoding.UTF8);
            LeaderBoardItem item = JsonUtility.FromJson<LeaderBoardItem>(jsonString);
            LeaderBoard.Add(item);
        }
    }

    private void FillLeaderBoard()
    {
        foreach (LeaderBoardItem item in LeaderBoard)
        {
            if (item.Score != 0)
            {
                LeaderBoardItemView newItemView = Instantiate<LeaderBoardItemView>(itemPrefab, content);
                newItemView.NameText.text = item.Name;
                newItemView.ScoreText.text = item.Score.ToString();
            }
        }
    }
}
