﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Визуальный элемент таблицы лучших игроков
/// </summary>
public class LeaderBoardItemView : MonoBehaviour
{
    /// <summary>
    /// Текстовое поле имени
    /// </summary>
    public Text NameText;
    /// <summary>
    /// Текстовое поле очков
    /// </summary>
    public Text ScoreText;
}
