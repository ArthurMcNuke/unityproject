﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ButtonVisualParams")]
public class ButtonVisualParams : ScriptableObject
{
    [SerializeField] Vector3 upScale;

    /// <summary>
    /// Воззврщает вектор для увеличения размера кнопки
    /// </summary>
    public Vector3 UpScale { get { return upScale; } }
}
