﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Button))]
public abstract class AbstractButtonView : MonoBehaviour
{
    protected Button btn;
    [SerializeField] ButtonVisualParams triggerEventParams;

    protected void Awake()
    {
        btn = GetComponent<Button>();
    }

    protected virtual void OnEnable()
    {
        btn.onClick.AddListener(OnButtonClicked);
    }

    protected virtual void OnDisable()
    {
        btn.onClick.RemoveListener(OnButtonClicked);
    }

    protected abstract void OnButtonClicked();

    /// <summary>
    /// Увеличивает размер кнопки
    /// </summary>
    public void ScaleUp()
    {
        btn.transform.localScale = triggerEventParams.UpScale;
    }

    /// <summary>
    /// Возвращает размер кнопки в состояние по умолчанию
    /// </summary>
    public void ScaleNormal()
    {
        btn.transform.localScale = Vector3.one;
    }
}
