﻿using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractTextView : MonoBehaviour
{
    protected Text text;
    protected void Awake()
    {
        text = GetComponent<Text>();
    }
}
