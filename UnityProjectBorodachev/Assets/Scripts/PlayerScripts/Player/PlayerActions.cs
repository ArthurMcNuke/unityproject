﻿using System;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    /// <summary>
    /// Действие выполняемое при нажатии на кнопку выстрела
    /// </summary>
    public static event Action OnShoot = delegate { };
    /// <summary>
    /// Действие выполняемое при нажатии на кнопку прыжка
    /// </summary>
    public static event Action OnJump = delegate { };
    /// <summary>
    /// Действие выполняемое при нажатии на кнопку Escape
    /// </summary>
    public static event Action OnEscapePressed = delegate { };

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            OnShoot.Invoke();

        if (Input.GetKeyDown(KeyCode.Space))
            OnJump.Invoke();

        if (Input.GetKeyDown(KeyCode.Escape))
            OnEscapePressed.Invoke();
    }
}
