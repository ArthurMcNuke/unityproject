﻿using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [SerializeField] Transform playerBodyTransform;
    private DataHolder data;

    float rotationX = 0.0f;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        data = FindObjectOfType<DataHolder>();
    }

    private void Update()
    {
        RotateCamera();
    }

    private void RotateCamera()
    {
        float rotationY = Input.GetAxis("Mouse X") * data.MouseSensitivity * Time.deltaTime;
        rotationX -= Input.GetAxis("Mouse Y") * data.MouseSensitivity * Time.deltaTime;
        rotationX = Mathf.Clamp(rotationX, -90.0f, 90.0f);
        playerBodyTransform.Rotate(Vector3.up * rotationY);
        transform.localRotation = Quaternion.Euler(rotationX, 0, 0);
    }
}
