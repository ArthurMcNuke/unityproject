﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Pistol playerGun;
    [SerializeField] HealthController playerHealth;
    [SerializeField] PlayerMovementSetter movementSetter;

    /// <summary>
    /// Здоровье игрока
    /// </summary>
    public HealthController Health { get { return playerHealth; } }
    /// <summary>
    /// Оружие игрока
    /// </summary>
    public Pistol Gun { get { return playerGun; } }
    /// <summary>
    /// Тип передвижения игрока
    /// </summary>
    public AbstractPlayerMovement MovementController { get { return movementSetter.Movement; } }
}
