﻿using UnityEngine;

public class PlayerMovementSetter : MonoBehaviour
{
    [SerializeField]
    bool moveByPhysics;

    AbstractPlayerMovement mov;

    private void Start()
    {
        if (moveByPhysics)
        {
            mov = gameObject.AddComponent<PlayerPhysicalMovement>();
        }
        else
        {
            mov = gameObject.AddComponent<PlayerMovement>();
        }
    }

    public AbstractPlayerMovement Movement { get { return mov; } }
}
