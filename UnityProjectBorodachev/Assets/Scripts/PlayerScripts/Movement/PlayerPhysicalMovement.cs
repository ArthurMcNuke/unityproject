﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerPhysicalMovement : AbstractPlayerMovement
{
    DataHolder data;
    Rigidbody rb;

    float xInput;
    float zInput;

    float playerDistanceCameraToFeet;

    private void OnEnable()
    {
        PlayerActions.OnJump += Jump;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        CapsuleCollider collider = GetComponent<CapsuleCollider>();
        playerDistanceCameraToFeet = collider.height * 0.9f;

        data = FindObjectOfType<DataHolder>();
    }

    private void Update()
    {
        xInput = Input.GetAxis("Horizontal");
        zInput = Input.GetAxis("Vertical");
    }

    private void FixedUpdate()
    {
        Move();
    }

    void OnDisable()
    {
        PlayerActions.OnJump -= Jump;
    }

    override public void Move()
    {
        Vector3 moveDirection = transform.TransformDirection(xInput, 0, zInput);
        moveDirection = moveDirection.normalized * data.MoveSpeed;
        moveDirection.y = rb.velocity.y;
        rb.velocity = moveDirection;
    }

    void Jump()
    {
        if (IsGrounded())
        {
            rb.AddForce(Vector3.up * data.JumpHeight, ForceMode.Impulse);
        }
    }

    bool IsGrounded()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), playerDistanceCameraToFeet))
        {
            return true;
        }
        else return false;
    }
}
