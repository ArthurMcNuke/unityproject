﻿using UnityEngine;

public class PlayerMovement : AbstractPlayerMovement
{
    [SerializeField] DataHolder data;
    private void Start()
    {
        moveSpeed = data.MoveSpeed;
    }

    private void Update()
    {
        Move();
    }

    override public void Move()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");
        Vector3 moveDirection = new Vector3(xInput, 0, zInput);
        transform.Translate(moveDirection.normalized * moveSpeed * Time.deltaTime);
    }
}
