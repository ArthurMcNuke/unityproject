﻿using UnityEngine;

/// <summary>
/// Класс для создания новых видов движения игрока
/// </summary>
public abstract class AbstractPlayerMovement : MonoBehaviour
{
    protected float moveSpeed;

    /// <summary>
    /// Должен реализовывать перемещение игрока по локации
    /// </summary>
    public abstract void Move();
}
