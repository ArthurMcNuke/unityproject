﻿using UnityEngine;

/// <summary>
/// Класс для создания новых атрибутов персонажа
/// </summary>
public abstract class AbstractPlayerCharachteristic : MonoBehaviour
{
    /// <summary>
    /// Увеличивает значение атрибута на значение ammount
    /// </summary>
    /// <param name="ammount"></param>
    public abstract void Decrease(int ammount);
    /// <summary>
    /// Уменьшает значение атрибута на значение ammount
    /// </summary>
    /// <param name="ammount"></param>
    public abstract void Increase(int ammount);
}
