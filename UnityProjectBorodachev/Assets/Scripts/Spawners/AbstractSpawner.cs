﻿using UnityEngine;

/// <summary>
/// Класс для создания новых видов сущностей порождающих врагов
/// </summary>
public abstract class AbstractSpawner : MonoBehaviour
{
    /// <summary>
    /// Создаёт врага
    /// </summary>
    /// <param name="pos">Место создания врага</param>
    /// <returns>Указатель на созданнаого врага</returns>
    public abstract AbstractEnemy SpawnEnemy(Vector3 pos);
}
