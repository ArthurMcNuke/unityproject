﻿using UnityEngine;
using System.Collections.Generic;

public class SimpleSpawner : AbstractSpawner
{
    [SerializeField] AbstractEnemy enemyPrefab;
    [SerializeField] int poolSize;
    private List<AbstractEnemy> enemyPool = new List<AbstractEnemy>();

    private void Awake()
    {
        CreateEnemyPool();
    }

    private void CreateEnemyPool()
    {
        for (int i = 0; i < poolSize; i++)
        {
            enemyPool.Add(Instantiate<AbstractEnemy>(enemyPrefab));
            enemyPool[i].gameObject.SetActive(false);
        }
    }

    public override AbstractEnemy SpawnEnemy(Vector3 pos)
    {
        for (int i = 0; i < enemyPool.Count; i++)
        {
            if (enemyPool[i].gameObject.activeInHierarchy == false)
            {
                enemyPool[i].Spawn(pos);
                return enemyPool[i];
            }
        }

        return null;
    }
}
