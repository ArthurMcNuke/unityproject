﻿using System;

/// <summary>
/// Класс управляющий здоровьем персонажа
/// </summary>
public class HealthController : AbstractPlayerCharachteristic
{
    /// <summary>
    /// Вызывается при изменении значения HealthPoints
    /// </summary>
    public event Action OnHPChanged = delegate { };

    /// <summary>
    /// Вызывается при значении HealthPoints <= 0
    /// </summary>
    public event Action OnDeath = delegate { };

    private void Start()
    {
        MaxHP = 100;
        HealthPoints = MaxHP;
    }

    /// <summary>
    /// Возвращает текущее значение здоровья персонажа
    /// </summary>
    public int HealthPoints { get; private set; }

    /// <summary>
    /// Возвращает максимальное значение здоровья персонажа
    /// </summary>
    public int MaxHP { get; private set; }

    /// <summary>
    /// Увеличивает здоровье персонажа на значение HP
    /// <para>Показатель здоровья не превышает значение MaxHP </para>
    /// </summary>
    /// <param name="HP"></param>
    public override void Increase(int HP)
    {
        HealthPoints += HP;
        if (HealthPoints > MaxHP)
        {
            HealthPoints = MaxHP;
        }
        OnHPChanged.Invoke();
    }

    /// <summary>
    /// Уменьшает здоровье персонажа на значение HP
    /// </summary>
    /// <para>Персонаж умерает, если значение HealthPoints <= 0 </para>
    /// <param name="HP"></param>
    public override void Decrease(int HP)
    {
        HealthPoints -= HP;
        if (HealthPoints <= 0)
        {
            HealthPoints = 0;
            OnDeath.Invoke();
        }

        OnHPChanged.Invoke();
    } 
}
