﻿using UnityEngine;

public class SimpleTarget: AbstractEnemy
{
    public override void Die()
    {
        gameObject.SetActive(false);
    }

    public override void Spawn(Vector3 pos)
    {
        transform.position = pos;
        gameObject.SetActive(true);
        health.Increase(health.MaxHP);
    }
}
