﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Класс для создания новых видов врагов
/// </summary>
[RequireComponent(typeof(HealthController))]
[RequireComponent(typeof(NavMeshAgent))]
public abstract class AbstractEnemy : MonoBehaviour
{
    protected HealthController health;
    protected NavMeshAgent agent;

    protected void Awake()
    {
        health = GetComponent<HealthController>();
        agent = GetComponent<NavMeshAgent>();
    }

    protected void OnEnable()
    {
        health.OnDeath += Die;
    }

    protected void OnDisable()
    {
        health.OnDeath -= Die;
    }

    /// <summary>
    /// Действия производимые при смерти врага
    /// </summary>
    public abstract void Die();

    /// <summary>
    /// Действия производимые при спавне врага на сцене
    /// </summary>
    /// <param name="pos">Место спавна</param>
    public abstract void Spawn(Vector3 pos);

    /// <summary>
    /// Возвращает ссылку на компонент HealthController
    /// </summary>
    public HealthController Health { get { return health; } }

    /// <summary>
    /// Возвращает ссылку на компонент NavMeshAgent
    /// </summary>
    public NavMeshAgent MeshAgent { get { return agent; } }
}
