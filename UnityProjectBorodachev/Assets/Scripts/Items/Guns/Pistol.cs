﻿using UnityEngine;

public class Pistol : AbstractGun
{
    [SerializeField] GunScriptable pistolData;

    private void Start()
    {
        ClipAmmo = pistolData.ClipSize;
        TotalAmmo = pistolData.InitialTotalAmmo;
        OnAmmoChanged.Invoke();
    }

    public override void Shoot()
    {
        if (ClipAmmo > 0)
        {
            ClipAmmo -= 1;
            if (Physics.Raycast(cam.transform.position, cam.transform.TransformDirection(Vector3.forward),
                out RaycastHit hit, 320.0f, 0xff, QueryTriggerInteraction.UseGlobal))
            {
                scoreCounter.CountHit(hit, pistolData.Damage);
            }
            OnAmmoChanged.Invoke();
        }
        else if (TotalAmmo > 0) Reload();
    }

    public override void Reload()
    {
        int missingAmmo = pistolData.ClipSize - ClipAmmo;
        if (missingAmmo > TotalAmmo)
        {
            ClipAmmo += TotalAmmo;
            TotalAmmo = 0;
        }
        else
        {
            ClipAmmo += missingAmmo;
            TotalAmmo -= missingAmmo;
        }

        OnAmmoChanged.Invoke();
    }
}
