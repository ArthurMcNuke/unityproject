﻿using UnityEngine;
using System;

/// <summary>
/// Абстрактный класс для создания новых видов оружия
/// </summary>
public abstract class AbstractGun : MonoBehaviour
{
    protected ScoreCounter scoreCounter;
    protected PlayerCamera cam;

    /// <summary>
    /// При изменении количества патронов
    /// </summary>
    public Action OnAmmoChanged = delegate { };

    /// <summary>
    /// Текущее количество патронов в обойме
    /// </summary>
    public int ClipAmmo { get; protected set; }

    /// <summary>
    /// Общее количество патронов для данного оружия
    /// </summary>
    public int TotalAmmo { get; protected set; }

    protected void Awake()
    {
        scoreCounter = FindObjectOfType<ScoreCounter>();
        cam = FindObjectOfType<PlayerCamera>();
    }

    protected virtual void OnEnable()
    {
        PlayerActions.OnShoot += Shoot;
    }

    protected virtual void OnDisable()
    {
        PlayerActions.OnShoot -= Shoot;
    }

    /// <summary>
    /// Производит выстрел из оружия
    /// <para>Должен определять характер стрельбы для данного оружия</para>
    /// </summary>
    public abstract void Shoot();

    /// <summary>
    /// Перезаряжает оружие с учётом имеющихся в обойме патронов
    /// <para>Должен пополнять текущее количество патронов в обойме данного оружия</para>
    /// </summary>
    public abstract void Reload();

    /// <summary>
    /// Увеличивает общее количество патронов на значение ammo
    /// </summary>
    /// <param name="ammo"> Количество добавляемых патронов</param>
    public void AddAmmo(int ammo)
    {
        TotalAmmo += ammo;
    }
}
