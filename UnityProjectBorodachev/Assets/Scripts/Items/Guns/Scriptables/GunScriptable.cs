﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/GunScriptable")]
public class GunScriptable : ScriptableObject
{
    [SerializeField] int clipSize;
    [SerializeField] int damage;
    [SerializeField] int intialTotalAmmo;

    /// <summary>
    /// Размер обоймы
    /// </summary>
    public int ClipSize { get { return clipSize; } }

    /// <summary>
    /// Наносимый урон
    /// </summary>
    public int Damage { get { return damage; } }

    /// <summary>
    /// Количество патронов на старте
    /// </summary>
    public int InitialTotalAmmo { get { return intialTotalAmmo; } }
}
